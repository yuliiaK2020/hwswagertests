package restApiTest.forStudents.src.test.java;

import main.java.entities.User.User;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestsHWApi {
    @Before
    public void setUp() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addUserToTheStore() {

        User newUser = new User(
                100000 + (long) (Math.random() * 999999), "Name88888", "FirstName", "LastName",
                "user@gmail.com", "11111111", "+38000000000",
                100000 + (long) (Math.random() * 999999));

        //        Tests
        Response responseAddUser = given()
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(newUser)
                .post();

        Assert.assertEquals("Wrong status code", 200, responseAddUser.getStatusCode());
        System.out.println("Response for adding a new user: \n" + responseAddUser.asString() + "\n");



        User foundUserByUserName = given()
                .pathParam("username", newUser.getUsername())
                .basePath("/user/{username}")
                .accept("application/json")
                .when()
                .get()
                .as(User.class);

        System.out.println("Response for getting user by userName: \n" + foundUserByUserName.toString());
        Assert.assertEquals("Wrong email", newUser.getEmail(), foundUserByUserName.getEmail());
        Assert.assertEquals("Wrong phone", newUser.getPhone(), foundUserByUserName.getPhone());
        Assert.assertEquals("Wrong FirstName", newUser.getFirstName(), foundUserByUserName.getFirstName());
        Assert.assertEquals("Wrong LastName", newUser.getLastName(), foundUserByUserName.getLastName());


    }

    @Test
    public void addLadyGagaToTheStore() {

        User LadyGaga = new User(
                100000 + (long) (Math.random() * 55), "MyUser2020", "Lady", "Gaga",
                "ladygaga@gmail.com", "0000000", "+38000999999",
                100000 + (long) (Math.random() * 65));

        //        Tests
        Response responseAddLadyGaga = given()
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(LadyGaga)
                .post();


        Assert.assertEquals("Wrong status code", 200, responseAddLadyGaga.getStatusCode());
        System.out.println("1Response for adding a new user: \n" + responseAddLadyGaga.asString() + "\n");

        User DadyGoga = new User(
                100000 + (long) (Math.random() * 55), "MyUser2020", "Dady", "Goga",
                "dadygoga@gmail.com", "0000000", "+38000999999",
                100000 + (long) (Math.random() * 65));


        //        Tests
        Response changeLadyGagaData = given()
                .basePath("/username")
                .contentType(ContentType.JSON)
                .body(DadyGoga)
                .put();

   Assert.assertEquals("Wrong status code", 404, changeLadyGagaData.getStatusCode());
        System.out.println("2Response for changing LadyGaga: \n" + changeLadyGagaData.asString() + "\n");

    Response getDadyGoga = (Response) given()
            .basePath("MyUser2020")
            .contentType(ContentType.JSON)
            .body(DadyGoga)
            .get();


       Assert.assertEquals("Wrong status code", 404,getDadyGoga.getStatusCode());
     ;

    }





}
