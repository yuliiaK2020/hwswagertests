package restApiTest.forStudents.src.test.java;

import main.java.entities.User.AddResponse;
import main.java.entities.User.DeleteResponseUser;
import main.java.entities.User.User;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static io.restassured.RestAssured.given;


public class UserTestsRest {

    @Before
    public void setUp() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetDeleteUser() {

        User newUser = new User(
                100000 + (long) (Math.random() * 999999),            //генерация рандомного id
                "Karl_Marks",
                "Karl",
                "Marks",
                "kmarks@gmail.com",
                "qwer1234",
                "+380500000000",
                100000 + (long) (Math.random() * 999999));           //генерация рандомного userStatus


        Response responseAddUser = given()                                //сохраняем добавленного юзера
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(newUser)                                            //передаем объект с параметрами
                .post();

        Assert.assertEquals("Wrong status code", 200, responseAddUser.getStatusCode());
        System.out.println("AddResponse for adding a new user: \n" + responseAddUser.asString() + "\n");     //получаем его в качестве строки
        AddResponse addedUser = responseAddUser.as(AddResponse.class);                                       //сохраняем в класс

        Response foundUserByUserName = given()                                              //проверка пользователя по id
                .pathParam("UserName", newUser.getUsername())
                .basePath("/user/{UserName}")                                          //работаем с переданным id
                .accept("application/json")
                .when()
                .get();

        System.out.println("AddResponse for getting user by UserName: \n" + foundUserByUserName.asString() + "\n");
        User foundUser = foundUserByUserName.as(User.class);
        Assert.assertEquals("Wrong Email", newUser.getEmail(), foundUser.getEmail());

        Response deleteResponse =                                                                 //запоминаем в ответе
                given()
                        .pathParam("UserName", foundUser.getUsername())
                        .basePath("/user/{UserName}")
                        .accept("application/json")                                              //проверка будет проверяться в формате json
                        .when()
                        .delete();

        System.out.println(deleteResponse.asString());

        DeleteResponseUser deleteResponseAsClass = deleteResponse.as(DeleteResponseUser.class);         //преобразовуем в класс после запроса всегда

        Assert.assertEquals("Code is wrong", 200, deleteResponseAsClass.getCode());
        Assert.assertNotNull("Field is null", deleteResponseAsClass.getType());
        Assert.assertEquals("Message is wrong", foundUser.getUsername(), deleteResponseAsClass.getMessage());

    }

    @Test
    public void validationJSONSchemaOfResponseGetUser() {
        User newUser = new User(
                100000 + (long) (Math.random() * 999999),            //генерация рандомного id
                "Karl_Marks",
                "Karl",
                "Marks",
                "kmarks@gmail.com",
                "qwer1234",
                "+380500000000",
                100000 + (long) (Math.random() * 999999));           //генерация рандомного userStatus

//        Tests
        Response responseAddUser = given()                                    //сохраняем добавленного юзера
                .basePath("/user")
                .contentType(ContentType.JSON)
                .body(newUser)                                                //передаем объект с параметрами
                .post();

        given()
                .basePath("/user/" + responseAddUser.getStatusCode())
                .accept("application/json")
                .get()
                .then()
                .assertThat()                                                 //обязательный атрибут
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("main/java/data/json/schema2.json"));
    }

}