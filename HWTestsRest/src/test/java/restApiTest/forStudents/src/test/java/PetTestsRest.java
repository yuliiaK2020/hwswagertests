package restApiTest.forStudents.src.test.java;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import main.java.data.Statuses;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import restApiTest.forStudents.src.main.java.entities.Pet.*;

import java.util.Arrays;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class PetTestsRest {

    @Before
    public void setUp() {
        RestAssured.baseURI = "https://petstore.swagger.io/v2";
    }

    @After
    public void tearDown() {
    }

    //1
    @Test
    public void addPetToTheStorewithNotWalidId() {
        Category elephant = new Category(555, "elephant");
        Category zooAnimal = new Category(111, "zooAnimal");
        PetNotValidData myPet = new PetNotValidData(
                "1111111111111111111111111",
                elephant,
                "Dambo" + RandomStringUtils.randomAlphabetic(5),
                Collections.singletonList("urls"),
                Arrays.asList(elephant, zooAnimal),
                main.java.data.Statuses.AVAILABLE.name());
        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(myPet)
                .post();
        Assert.assertEquals("Wrong status code", 500, responseAddPet.getStatusCode());
    }

//2

    @Test
    public void addMyPet() {
//        Preparing test data
        Category cat = new Category(111, "cat");
        Category homeCat = new Category(222, "homeCat");

        Pet myPet = new Pet(
                100000 + (long) (Math.random() * 999999),
                cat,
                "Mozart " + RandomStringUtils.randomAlphabetic(5),
                Collections.singletonList("urls"),
                Arrays.asList(cat, homeCat),
                main.java.data.Statuses.AVAILABLE.name());

//        Tests
        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(myPet)
                .post();

        Assert.assertEquals("Wrong status code", 200, responseAddPet.getStatusCode());
        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString() + "\n"); // log info
        Pet myAddedCat = responseAddPet.as(Pet.class);

        Pet myPet1 = new Pet(
                myPet.getId(),
                cat,
                "Coconut",
                Collections.singletonList("urls"),
                Arrays.asList(cat, homeCat),
                Statuses.SOLD.name());

        Response changeNameAndId = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(myPet1)
                .put();
        System.out.println("PUTResponse for changing pet Name Id: \n" + changeNameAndId.toString()); // log info
        changeNameAndId.as(Pet.class);

        Pet getChangesToCheck = given()
                .pathParam("Id", myPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get()
                .as(Pet.class);

        Assert.assertEquals("Wrong name", myPet1.getName(), getChangesToCheck.getName());
        Assert.assertEquals("Wrong status", myPet1.getStatus(), getChangesToCheck.getStatus());

    }

    //4
    @Test
    public void createPetDeleteAndCheck() {
//        Preparing test data
        Category cat = new Category(111, "cat");
        Category homeCat = new Category(222, "homeCat");

        Pet myPet = new Pet(
                100000 + (long) (Math.random() * 999999),
                cat,
                "Cristopher " + RandomStringUtils.randomAlphabetic(5),
                Collections.singletonList("urls"),
                Arrays.asList(cat, homeCat),
                main.java.data.Statuses.AVAILABLE.name());

//        Tests
        Response responseAddPet = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(myPet)
                .post();

        Assert.assertEquals("Wrong status code", 200, responseAddPet.getStatusCode());
        System.out.println("Response for adding a new pet: \n" + responseAddPet.asString() + "\n"); // log info
        Pet createdPet = responseAddPet.as(Pet.class);

        Response deleteResponse = given()
                .pathParam("Id", createdPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .delete();
        System.out.println(deleteResponse.asString());
        DeleteResponse deleteResponseAsClass = deleteResponse.as(DeleteResponse.class);

        Assert.assertEquals("Code is wrong", 200, deleteResponseAsClass.getCode());

        given()
                .pathParam("Id", myPet.getId())
                .basePath("/pet/{Id}")
                .accept("application/json")
                .when()
                .get()
                .then()
                .body("message", equalTo("Pet not found"))
                .and()
                .log()
                .body();
    }

    //5 - not ready yet
    // Получить в GET запросе всех питомцем со статусом sold, проверить,
    // что там присутствует питомец с нашим ID,
    // проверить его имя на соответствие тому, которое мы указывали при добавлении.
    @Test
    public void addSoldPet() {

        Category dog = new Category(112, "dog");
        Category homeDog = new Category(2, "homeDog");

        Pet mySoldDog = new Pet(
                100000 + (long) (Math.random() * 999999),
                dog,
                "Bethoven",
                Collections.singletonList("urls"),
                Arrays.asList(dog, homeDog),
                new StatusesPetStrings().getS3());

        Response responseAddSoldDog = given()
                .basePath("/pet")
                .contentType(ContentType.JSON)
                .body(mySoldDog)
                .post();

        Assert.assertEquals("Wrong status code", 200, responseAddSoldDog.getStatusCode());
        System.out.println("Response for adding a new pet: \n" + responseAddSoldDog.asString() + "\n"); // log info
        //Pet myAddedSoldDog = responseAddSoldDog.as(Pet.class);

        Pet[] getSoldPets = given()
                .basePath("/pet/findByStatus?status=sold")
                .accept("application/json")
                .when()
                .get()
                .as(Pet[].class);
        System.out.println(getSoldPets.toString());
        //System.out.println(getSoldPets[0]);

        for (int i = 0; i < getSoldPets.length; i++) {
            if (getSoldPets[i].getId() == mySoldDog.getId()) {
                System.out.println(getSoldPets[i]);
                // Assertions.assertEquals(getSoldPets[i].getName(), mySoldDog.getName());
                //System.out.println("Your pet name" + getSoldPets[i].getName());
            } else {
                continue;
            }

        }

    }
}

