package main.java.entities.Order;
import lombok.*;

import java.time.OffsetDateTime;
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class Order {
    private long id;
    private long petID;
    private long quantity;
    private OffsetDateTime shipDate;
    private String status;
    private boolean complete;

//    public Order(){}
//
//    public Order(long id, long petID, long quantity, OffsetDateTime shipDate, String status, boolean complete) {
//        this.id = id;
//        this.petID = petID;
//        this.quantity = quantity;
//        this.shipDate = shipDate;
//        this.status = status;
//        this.complete = complete;
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public long getPetID() {
//        return petID;
//    }
//
//    public long getQuantity() {
//        return quantity;
//    }
//
//    public OffsetDateTime getShipDate() {
//        return shipDate;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public boolean isComplete() {
//        return complete;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public void setPetID(long petID) {
//        this.petID = petID;
//    }
//
//    public void setQuantity(long quantity) {
//        this.quantity = quantity;
//    }
//
//    public void setShipDate(OffsetDateTime shipDate) {
//        this.shipDate = shipDate;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public void setComplete(boolean complete) {
//        this.complete = complete;
//    }
//
//    @Override
//    public String toString() {
//        return "Order{" +
//                "id=" + id +
//                ", petID=" + petID +
//                ", quantity=" + quantity +
//                ", shipDate=" + shipDate +
//                ", status='" + status + '\'' +
//                ", complete=" + complete +
//                '}';
//    }
}
