package restApiTest.forStudents.src.main.java.entities.Pet;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode

public class StatusesPetStrings {
    private String s1 = "avaliable";
    private String s2 = "pending";
    private String s3 = "sold";

}
