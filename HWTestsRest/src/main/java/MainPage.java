import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class MainPage {
    MainPage (){
        PageFactory.initElements(WebDriverST.getDriver(), this);
    }

    @FindBy(css="[class='sn_menu'] :nth-child(2) a")
    private WebElement composeLink;

    @FindBy(css=".list_underlined ins")
    private WebElement lettersQuantity;

//t2
    @FindBy(css = "[class='attr'] [name='list[]']")
    private WebElement chooseFirstLetter;

    @FindBy(css = "#fieldset1 > fieldset:nth-child(2) > span.button.tl_bl")
    private WebElement mark;
    @FindBy(css = "#m_mark > div > ul > li:nth-child(2) > span")
    private WebElement asREad;

     @FindBy(css=".user_name")
     private WebElement userName;

    @FindBy(css="body > div.body_container > div.section_nav > ul > li:nth-child(3) > a")
    private WebElement contacts;






}
