import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class ComposePage {
    ComposePage (){
        PageFactory.initElements(WebDriverST.getDriver(), this);
    }

    @FindBy(css="#to")
    private WebElement recipient;

    @FindBy(css="[name='subject']")
    private WebElement subject;

    @FindBy(css="[class='text_editor_browser'] [name='body']")
    private WebElement letterText;

    @FindBy(css="body > div.body_container > div.Body > div.Cols_80_20.message_container > div.Left > p:nth-child(1) > input.bold")
    private WebElement sendButton;
}
